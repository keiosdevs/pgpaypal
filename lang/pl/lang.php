<?php

return [
    'labels' => [
        'pluginDesc' => 'Obsługa PayPal dla Keios PaymentGateway',
    ],
    'operators' => [
        'paypal_payments' => 'Płatności PayPal',
    ],
    'settings' => [
        'posId' => 'Twój PayPal ID',
        'posSecret' => 'Twój PayPal Secret',
        'mode' => 'Tryb',
        'logEnabled' => 'Włącz logowanie do pliku',
        'logFile' => 'Ścieżka do pliku logów',
        'logLevel' => 'Poziom logowania',
        'validationLevel' => 'Wykrywanie zmian w wersji PayPal API',
        'cache' => 'Używaj cachu',
        'connectTimeout' => 'Limit oczekiwania na połączenie (w sekundach)',
        'useAttributionId' => 'Użyj PayPal Partner Attribution',
        'attributionId' => 'ID PayPal Partner Attribution',
        'tab' => 'PayPal',
        'general' => 'Ustawienia ogólne',
        'advanced' => 'Ustawienia zaawansowane',
        'sandbox' => 'Tryb testowy',
        'live' => 'Tryb live',
        'logLevelDebug' => 'Tryb debugowania',
        'logLevelFine' => 'Pełne logowanie',
        'logLevelInfo' => 'Informacyjne logowanie',
        'logLevelWarn' => 'Tylko ostrzeżenia i błędy',
        'logLevelError' => 'Tylko błędy',
        'partner' => 'Ustawienia partnera PayPal',
        'validationLog' => 'Loguj wszystkie problemy z API PayPala, ale nie zatrzymuj aplikacji',
        'validationStrict' => 'Zatrzymaj aplikację przy każdym konflikcie wersji API, niebezpieczne - używaj tylko przy debugu!',
        'validationDisable' => 'Wyłącz sprawdzanie wersji API',
    ],
    'info' => [
        'header' => 'Skąd wziąć dane dla obsługi płatności PayPal',
    ],
];