<?php

return [
    'labels' => [
        'pluginDesc' => 'Paypal Integration Extension for Keios PaymentGateway',
    ],
    'operators' => [
        'paypal_payments' => 'PayPal Payments',
    ],
    'settings' => [
        'posId' => 'Your PayPal ID',
        'posSecret' => 'Your PayPal Secret',
        'mode' => 'Mode',
        'logEnabled' => 'Enable logging to file',
        'logFile' => 'Path to logfile',
        'logLevel' => 'Log level',
        'validationLevel' => 'PayPal API version change detection',
        'cache' => 'Use caching',
        'connectTimeout' => 'Connection Timeout (in seconds)',
        'useAttributionId' => 'Use PayPal Partner attribution',
        'attributionId' => 'PayPal Partner Attribution ID',
        'tab' => 'PayPal',
        'general' => 'General settings',
        'advanced' => 'Advanced settings',
        'sandbox' => 'Test mode',
        'live' => 'Live mode',
        'logLevelDebug' => 'Debug mode, everything is logged, downgrades to full in live mode',
        'logLevelFine' => 'Full logging',
        'logLevelInfo' => 'Informative logging',
        'logLevelWarn' => 'Only warnings and errors',
        'logLevelError' => 'Only errors',
        'partner' => 'PayPal Partner Settings',
        'validationLog' => 'Log all problems with PayPal API, but don\'t stop application',
        'validationStrict' => 'Stop application on every API version mismatch, dangerous, for debug only',
        'validationDisable' => 'Disable all PayPal API version checks',
    ],
    'info' => [
        'header' => 'How to retrieve your PayPal credentials',
    ],
];