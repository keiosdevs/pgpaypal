<?php namespace Keios\PGPayPal\Facades;

use October\Rain\Support\Facade;

class ContextMaker extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'keios.pgpaypal.contextMaker';
    }
}