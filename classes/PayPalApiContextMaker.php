<?php namespace Keios\PGPayPal\Classes;

use Keios\PaymentGateway\Models\Settings;
use PayPal\Auth\OAuthTokenCredential;
use Illuminate\Cache\CacheManager;
use PayPal\Rest\ApiContext;

/**
 * Class PayPalApiContextMaker
 *
 * @package Keios\PGPayPal
 */
class PayPalApiContextMaker
{
    /**
     * @var
     */
    protected $apiContext;

    /**
     * @var
     */
    protected $settings;

    /**
     * @var array
     */
    protected $contextConfig = [];

    /**
     * @var
     */
    protected $posId;

    /**
     * @var
     */
    protected $posSecret;

    /**
     * @var bool
     */
    protected $booted = false;

    /**
     * @param \Illuminate\Cache\CacheManager $cache
     */
    public function __construct(CacheManager $cache)
    {
        $this->settings = Settings::instance();

        $this->cache = $cache;
    }

    /**
     * @return \PayPal\Rest\ApiContext
     */
    public function getContext()
    {
        if (!$this->booted) {
            $this->buildContextConfig();

            $this->createContext();

            $this->booted = true;
        }

        return $this->apiContext;
    }

    /**
     * @return null
     */
    protected function buildContextConfig()
    {
        if ($this->cache->has('keios.pgpaypal.cacheFile')) {
            $cachePath = storage_path().'/temp/'.$this->cache->get('keios.pgpaypal.cacheFile');
        } else {
            $file = uniqid('KPG_PAYPAL_AUTHCACHE_', true);
            $cachePath = storage_path().'/temp/'.$file;
            $this->cache->forever('keios.pgpaypal.cacheFile', $file);
        }

        $this->posId = $this->settings->get('paypal.posId', '');
        $this->posSecret = $this->settings->get('paypal.posSecret', '');

        $this->contextConfig['mode'] = $this->settings->get('paypal.mode', 'sandbox');
        $this->contextConfig['log.logEnabled'] = $this->settings->get('paypal.logEnabled', true);
        $this->contextConfig['log.FileName'] = $this->settings->get(
            'paypal.logFile',
            storage_path().'/logs/PayPal.log'
        );
        $this->contextConfig['log.LogLevel'] = $this->settings->get('paypal.logLevel', 'DEBUG');
        $this->contextConfig['validation.level'] = $this->settings->get(
            'paypal.validationLevel',
            'log'
        );
        $this->contextConfig['cache.enabled'] = $this->settings->get('paypal.cache', true);
        $this->contextConfig['http.CURLOPT_CONNECTTIMEOUT'] = $this->settings->get('paypal.connectTimeout', 30);

        if ($this->contextConfig['mode'] === 'live') {
            $this->contextConfig['service.EndPoint'] = 'https://api.paypal.com';
        } else {
            $this->contextConfig['service.EndPoint'] = 'https://api.sandbox.paypal.com';
        }

        if ($this->contextConfig['cache.enabled']) {
            $this->contextConfig['cache.FileName'] = $cachePath;
        }

        if ($this->settings->get('paypal.useAttributionId', false) && $this->settings->get('paypal.attributionId')) {
            $this->contextConfig['http.headers.PayPal-Partner-Attribution-Id'] = $this->settings->get(
                'paypal.attributionId'
            );
        }
    }

    /**
     * @return null
     */
    protected function createContext()
    {
        $this->apiContext = new ApiContext(new OAuthTokenCredential($this->posId, $this->posSecret));

        $this->apiContext->setConfig($this->contextConfig);
    }
}