<?php namespace Keios\PGPayPal\Classes;

use Keios\PaymentGateway\ValueObjects\Details as PGDetails;
use Keios\PaymentGateway\Support\OperatorUrlizer;
use Keios\PaymentGateway\Core\Operator;
use Keios\PaymentGateway\Contracts\Orderable;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;
use PayPal\Api\ItemList;
use PayPal\Api\Details;
use PayPal\Api\Payment;
use PayPal\Api\Amount;
use PayPal\Api\Payer;
use PayPal\Api\Item;

/**
 * Class PayPalPaymentBuilder
 *
 * @package Keios\PGPayPal
 */
class PayPalPaymentBuilder
{
    /**
     * @var \Keios\PaymentGateway\ValueObjects\Cart
     */
    protected $cart;

    /**
     * @var \Keios\PaymentGateway\ValueObjects\Details
     */
    protected $details;

    /**
     * @var \Keios\PaymentGateway\Core\Operator
     */
    protected $payment;


    /**
     * @param \Keios\PaymentGateway\Contracts\Orderable  $cart
     * @param \Keios\PaymentGateway\ValueObjects\Details $details
     * @param \Keios\PaymentGateway\Core\Operator  $payment
     */
    public function __construct(Orderable $cart, PGDetails $details, Operator $payment)
    {
        $this->cart = $cart;
        $this->details = $details;
        $this->payment = $payment;
    }

    /**
     * @return \PayPal\Api\Payment
     */
    public function getPayment()
    {
        $payment = new Payment();
        $payment->setIntent("sale")
                ->setPayer($this->makePayer())
                ->setRedirectUrls($this->makeRedirectUrls())
                ->setTransactions([$this->makeTransaction()]);

        return $payment;
    }

    /**
     * @return \PayPal\Api\RedirectUrls
     */
    protected function makeRedirectUrls()
    {
        $redirectUrls = new RedirectUrls();

        $redirectUrls->setReturnUrl(
            \URL::to(
                '_paymentgateway/'.OperatorUrlizer::urlize($this->payment).
                '?success=1&pgUuid='.base64_encode($this->payment->uuid)
            )
        );

        $redirectUrls->setCancelUrl(
            \URL::to(
                '_paymentgateway/'.OperatorUrlizer::urlize($this->payment).
                '?success=0&pgUuid='.base64_encode($this->payment->uuid)
            )
        );

        return $redirectUrls;
    }

    /**
     * @return \PayPal\Api\Payer
     */
    protected function makePayer()
    {
        $payer = new Payer();
        $payer->setPaymentMethod('paypal');

        return $payer;
    }

    /**
     * @return array
     */
    protected function makeItems()
    {
        $items = [];
        /**
         * @var \Keios\PaymentGateway\ValueObjects\Item $cartItem
         */
        foreach ($this->cart as $cartItem) {
            $item = new Item();

            $item->setName($cartItem->getDescription())
                 ->setCurrency($cartItem->getCurrency())
                 ->setQuantity($cartItem->getCount())
                 ->setPrice($cartItem->getSingleNetPrice()->getAmountBasic())
                 ->setTax($cartItem->getSingleNetPrice()->multiply($cartItem->getTax() / 100)->getAmountBasic());

            $items[] = $item;
        }

        return $items;
    }

    /**
     * @return \PayPal\Api\ItemList
     */
    protected function makeItemList()
    {
        $itemList = new ItemList();
        $itemList->setItems($this->makeItems());

        return $itemList;
    }

    /**
     * @return \PayPal\Api\Details
     */
    protected function makeDetails()
    {
        $details = new Details();

        $taxValue = $this->cart->getSubTotalTaxValue()->getAmountBasic();

        $details->setTax($taxValue)
                ->setSubtotal($this->cart->getSubTotalNetCost()->getAmountBasic());

        if ($this->cart->hasShipping()) {
            $details->setShipping($this->cart->getShipping()->getGrossCost()->getAmountBasic());
        }

        return $details;
    }

    /**
     * @return \PayPal\Api\Transaction
     */
    protected function makeTransaction()
    {
        $transaction = new Transaction();
        $transaction->setAmount($this->makeAmount())
                    ->setItemList($this->makeItemList())
                    ->setDescription($this->details->getDescription())
                    ->setInvoiceNumber($this->payment->uuid);

        return $transaction;
    }

    /**
     * @return \PayPal\Api\Amount
     */
    protected function makeAmount()
    {
        $amount = new Amount();
        $amount->setCurrency($this->cart->getTotalNetCost()->getCurrency()->getIsoCode())
               ->setTotal($this->cart->getTotalGrossCost()->getAmountBasic())
               ->setDetails($this->makeDetails());

        return $amount;
    }
}