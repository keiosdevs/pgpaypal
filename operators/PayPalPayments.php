<?php namespace Keios\PGPayPal\Operators;

use Finite\StatefulInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Keios\PaymentGateway\ValueObjects\PaymentResponse;
use Keios\PGPayPal\Classes\PayPalPaymentBuilder;
use PayPal\Exception\PayPalConnectionException;
use Keios\PaymentGateway\Core\Operator;
use PayPal\Api\PaymentExecution;
use PayPal\Api\Payment;
use PayPal\Api\Amount;
use PayPal\Api\Refund;
use PayPal\Api\Sale;

/**
 * Class PayPalPayments
 *
 * @package Keios\PGPayPal
 */
class PayPalPayments extends Operator implements StatefulInterface
{
    /**
     * @var string
     */
    public static $operatorCode = 'keios.pgpaypal::lang.operators.paypal_payments';

    /**
     * @var string
     */
    public static $operatorLogoPath = '/plugins/keios/pgpaypal/assets/img/paypal/logo.png';

    /**
     * @var string
     */
    public static $modeOfOperation = 'api';

    /**
     * @var array
     */
    public static $configFields = [];

    /**
     * @return \Keios\PaymentGateway\ValueObjects\PaymentResponse
     */
    public function sendPurchaseRequest()
    {
        $context = \ContextMaker::getContext();
        $paymentBuilder = new PayPalPaymentBuilder($this->cart, $this->paymentDetails, $this);

        $payment = $paymentBuilder->getPayment();

        try {

            $payment->create($context);

        } catch (PayPalConnectionException $pce) {

            $errors = $this->retrieveErrorsFromConnectionException($pce);

            return new PaymentResponse($this, null, $errors);

        } catch (\Exception $ex) {

            return new PaymentResponse($this, null, [$ex->getMessage()]);
        }

        $redirectUrl = $payment->getApprovalLink();

        $this->paypalId = $payment->getId(); // store paypal payment id in data
        
        return new PaymentResponse($this, $redirectUrl); // return redirect response to paypal confirmation page
    }

    /**
     * @param array $data
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function processNotification(array $data)
    {
        if (isset($data['success'])) {
            return $this->handleReturnFromApi($data);
        } else {
            return \Redirect::to('/error'); // todo other notifications
        }
    }

    /**
     * @return \Keios\PaymentGateway\ValueObjects\PaymentResponse
     */
    public function sendRefundRequest()
    {
        $context = \ContextMaker::getContext();

        $currency = $this->amount->getCurrency()->getIsoCode();

        $amount = $this->amount->getAmountBasic();

        $payPalAmount = new Amount();
        $payPalAmount->setCurrency($currency)
            ->setTotal($amount);

        $refund = new Refund();
        $refund->setAmount($payPalAmount);

        $sale = new Sale();
        $sale->setId($this->saleId);

        try {

            $sale->refund($refund, $context);

        } catch (PayPalConnectionException $pce) {

            $errors = $this->retrieveErrorsFromConnectionException($pce);

            return new PaymentResponse($this, null, $errors);

        } catch (\Exception $ex) {

            return new PaymentResponse($this, null, [$ex->getMessage()]);
        }

        return new PaymentResponse($this, null);
    }

    /**
     * @param array $data
     *
     * @return string
     */
    public static function extractUuid(array $data)
    {
        if (isset($data['pgUuid'])) {
            return base64_decode($data['pgUuid']);
        } else {
            throw new \RuntimeException('Invalid redirect, payment uuid is missing.');
        }
    }

    /**
     * @param array $data
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function handleReturnFromApi(array $data)
    {
        if ($data['success']) {

            $context = \ContextMaker::getContext();

            if (!$this->can(self::TRANSITION_REJECT) && !$this->can(self::TRANSITION_ACCEPT)) {
                return \Redirect::to($this->returnUrl);                // todo redirect where?
            }

            $paymentId = $data['paymentId'];

            try {

                $payment = Payment::get($paymentId, $context);

                $execution = new PaymentExecution();
                $execution->setPayerId($data['PayerID']);

                $this->payerId = $data['PayerID'];

                $payment->execute($execution, $context);

                $payment = Payment::get($paymentId, $context);

            } catch (PayPalConnectionException $pce) {

                $errors = $this->retrieveErrorsFromConnectionException($pce);

                $this->logErrors($errors);

                $this->rejectFromApi();

                return \Redirect::to($this->returnUrl);

            } catch (\Exception $ex) {

                $this->rejectFromApi();

                return \Redirect::to($this->returnUrl);
                // todo redirect
            }

            $transactions = $payment->getTransactions();
            $relatedResources = $transactions[0]->getRelatedResources();
            $sale = $relatedResources[0]->getSale();

            $this->saleId = $sale->getId();

            if ($payment->getState() === 'approved' || $payment->getState() === 'complete') {
                $this->accept();
            } else {
                $this->rejectFromApi();
            }

            return \Redirect::to($this->returnUrl); // todo

        } else {
            if ($this->can(self::TRANSITION_CANCEL)) {
                $this->cancel();
            }

            return \Redirect::to($this->returnUrl); //todo
        }
    }

    protected function retrieveErrorsFromConnectionException(PayPalConnectionException $pce)
    {
        $errors = [];

        $data = json_decode($pce->getData());

        if (is_object($data) && property_exists($data, 'details')) {
            foreach ($data->details as $error) {
                $errors[] = $error->field.': '.$error->issue;
            }
        } elseif (is_object($data) && property_exists($data, 'message')) {
            $errors[] = $data->message;
        } else {
            $errors[] = $pce->getMessage();
        }

        return $errors;
    }
}
