<?php namespace Keios\PGPayPal;

use Illuminate\Foundation\AliasLoader;
use Keios\PaymentGateway\Models\Settings as PaymentGatewaySettings;
use Keios\PaymentGateway\Models\Settings;
use Keios\PGPayPal\Classes\PayPalApiContextMaker;
use System\Classes\PluginBase;
use Event;

/**
 * PG-Paypal Plugin Information File
 *
 * @package Keios\PGPayPal
 */
class Plugin extends PluginBase
{
    public $require = [
        'Keios.PaymentGateway'
    ];

    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name' => 'PG-Paypal',
            'description' => 'keios.pgpaypal::lang.labels.pluginDesc',
            'author' => 'Keios',
            'icon' => 'icon-paypal'
        ];
    }

    public function register()
    {
        $this->app->singleton(
            'keios.pgpaypal.contextMaker',
            function () {
                return new PayPalApiContextMaker($this->app->make('cache'));
            }
        );

        $alias = AliasLoader::getInstance();
        $alias->alias('ContextMaker', 'Keios\PGPayPal\Facades\ContextMaker');

        Settings::extend(
            function (Settings $settings) {
                $settings->addDynamicMethod(
                    'getPaypal.modeOptions',
                    function () {
                        return [
                            'sandbox' => trans('keios.pgpaypal::lang.settings.sandbox'),
                            'live' => trans('keios.pgpaypal::lang.settings.live')
                        ];
                    }
                );

                $settings->addDynamicMethod(
                    'getPaypal.logLevelOptions',
                    function () {
                        return [
                            'DEBUG' => trans('keios.pgpaypal::lang.settings.logLevelDebug'),
                            'FINE' => trans('keios.pgpaypal::lang.settings.logLevelFine'),
                            'INFO' => trans('keios.pgpaypal::lang.settings.logLevelInfo'),
                            'WARN' => trans('keios.pgpaypal::lang.settings.logLevelWarn'),
                            'ERROR' => trans('keios.pgpaypal::lang.settings.logLevelError'),
                        ];
                    }
                );

                $settings->addDynamicMethod(
                    'getPaypal.validationLevelOptions',
                    function () {
                        return [
                            'log' => trans('keios.pgpaypal::lang.settings.validationLog'),
                            'strict' => trans('keios.pgpaypal::lang.settings.validationStrict'),
                            'disable' => trans('keios.pgpaypal::lang.settings.validationDisable'),
                        ];
                    }
                );
            }
        );

        Event::listen(
            'paymentgateway.booted',
            function () {
                /**
                 * @var \October\Rain\Config\Repository $config
                 */
                $config = $this->app['config'];
                $config->push('keios.paymentgateway.operators', 'Keios\PGPayPal\Operators\PayPalPayments');
            }
        );

        Event::listen(
            'backend.form.extendFields',
            function ($form) {

                if (!$form->model instanceof PaymentGatewaySettings) {
                    return;
                }

                if ($form->context !== 'general') {
                    return;
                }

                /**
                 * @var \Backend\Widgets\Form $form
                 */
                $form->addTabFields(
                    [
                        'paypal.general' => [
                            'label' => 'keios.pgpaypal::lang.settings.general',
                            'tab' => 'keios.pgpaypal::lang.settings.tab',
                            'type' => 'section',
                        ],
                        'paypal.info' => [
                            'type' => 'partial',
                            'path' => '$/keios/pgpaypal/partials/_paypal_info.htm',
                            'tab' => 'keios.pgpaypal::lang.settings.tab',
                        ],
                        'paypal.posId' => [
                            'label' => 'keios.pgpaypal::lang.settings.posId',
                            'tab' => 'keios.pgpaypal::lang.settings.tab',
                            'type' => 'text',
                            'default' => ''
                        ],
                        'paypal.posSecret' => [
                            'label' => 'keios.pgpaypal::lang.settings.posSecret',
                            'tab' => 'keios.pgpaypal::lang.settings.tab',
                            'type' => 'text',
                            'default' => ''
                        ],
                        'paypal.mode' => [
                            'label' => 'keios.pgpaypal::lang.settings.mode',
                            'tab' => 'keios.pgpaypal::lang.settings.tab',
                            'type' => 'dropdown',
                            'default' => 'sandbox'
                        ],
                        'paypal.advanced' => [
                            'label' => 'keios.pgpaypal::lang.settings.advanced',
                            'tab' => 'keios.pgpaypal::lang.settings.tab',
                            'type' => 'section',
                        ],
                        'paypal.logEnabled' => [
                            'label' => 'keios.pgpaypal::lang.settings.logEnabled',
                            'tab' => 'keios.pgpaypal::lang.settings.tab',
                            'type' => 'switch',
                        ],
                        'paypal.cache' => [
                            'label' => 'keios.pgpaypal::lang.settings.cache',
                            'tab' => 'keios.pgpaypal::lang.settings.tab',
                            'type' => 'switch',
                        ],
                        'paypal.logFile' => [
                            'label' => 'keios.pgpaypal::lang.settings.logFile',
                            'tab' => 'keios.pgpaypal::lang.settings.tab',
                            'placeholder' => storage_path().'/logs/PayPal.log',
                        ],
                        'paypal.logLevel' => [
                            'label' => 'keios.pgpaypal::lang.settings.logLevel',
                            'tab' => 'keios.pgpaypal::lang.settings.tab',
                            'type' => 'dropdown',
                            'default' => 'DEBUG'
                        ],
                        'paypal.validationLevel' => [
                            'label' => 'keios.pgpaypal::lang.settings.validationLevel',
                            'tab' => 'keios.pgpaypal::lang.settings.tab',
                            'type' => 'dropdown',
                            'default' => 'log'
                        ],
                        'paypal.connectTimeout' => [
                            'label' => 'keios.pgpaypal::lang.settings.connectTimeout',
                            'tab' => 'keios.pgpaypal::lang.settings.tab',
                            'type' => 'number',
                            'default' => '30'
                        ],
                        'paypal.partner' => [
                            'label' => 'keios.pgpaypal::lang.settings.partner',
                            'tab' => 'keios.pgpaypal::lang.settings.tab',
                            'type' => 'section',
                        ],
                        'paypal.useAttributionId' => [
                            'label' => 'keios.pgpaypal::lang.settings.useAttributionId',
                            'tab' => 'keios.pgpaypal::lang.settings.tab',
                            'type' => 'switch',
                            'default' => false
                        ],
                        'paypal.attributionId' => [
                            'label' => 'keios.pgpaypal::lang.settings.attributionId',
                            'tab' => 'keios.pgpaypal::lang.settings.tab',
                            'type' => '',
                            'trigger' => [
                                'action' => 'show',
                                'field' => 'paypal.useAttributionId',
                                'condition' => 'checked'
                            ]
                        ],
                    ]
                );
            }
        );
    }
}
